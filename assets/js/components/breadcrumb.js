const Breadcrumb = {
  props: {
    'breadcrumb': Array
  },
  template: `
    <nav class="breadcrumb" aria-label="Fil d’Ariane">
      <ol>
        <li v-for="crumb in breadcrumb">
          <a class="underline" :href="crumb.url":aria-current="crumb.isActive ? 'page': false">{{ crumb.title }}</a>
        </li>
      </ol>
    </nav>
  `
}

export default Breadcrumb