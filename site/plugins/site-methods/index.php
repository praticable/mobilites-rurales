<?php 

Kirby::plugin('my/plugin', [
  'siteMethods' => [
      'logintarget' => function () {
        $kirby = kirby();
        $user = $kirby->user();

        session_start();
        $redirectUrl = $user->role() == 'author' && isset($_SESSION["redirect_url"]) ? $_SESSION["redirect_url"] : "/panel";
        return $redirectUrl;
      }
  ]
]);