<?php

function getRecursiveTitleAndUrl($page, $template) {  
  if ($page->title()->value() != 'json' && (string)$page->template() === $template) {
    $children = null;
    if ($page->hasChildren() == 'true') {      
      foreach ($page->children()->listed() as $child) {
        if ($child->title()->value() != 'json') {
          $children[] = getRecursiveTitleAndUrl($child, $template);
        }
      }
    }
    $titleAndUrl = [
      'title' => $page->title()->value(),
      'url' =>  $page->isExternal() == 'true' ? $page->externalUrl()->value() : $page->url(),
      'isExternal' => $page->isExternal()->value() == true ? true : false,
      'children' => $children,
      'isDesktopOnly' => $page->isDesktopOnly()->isNotEmpty() ? 
        $page->isDesktopOnly()->value() == 'true' ? true: false
        : false
    ];
    return $titleAndUrl;
  }
}