<?php
function getItemByKeyValue($key, $value, $blocks) {
  foreach ($blocks as $block) {
    $isKeyExists = array_key_exists($key, $block['content']);
    
    if ($isKeyExists && $block['content'][$key] === $value) {
      return $block;
      break;
    }
  }
}