<?php

function cleanHTML($html) {
  $html_tag = '/<html>|<\/html>/';
  $head_tag = '/<head>(.+?)<\/head>/ms';
  $body_tag = '/<body>|<\/body>/';

  $html = preg_replace($html_tag, '', $html);
  $html = preg_replace($head_tag, '', $html);
  $html = preg_replace($body_tag, '', $html);

  return $html;
}