<?php
function prepareKBlockFromNotionBlock($notionBlock, $api, $page, $target) {
  $preparedBlock = null;
  switch ($notionBlock->type) {
    case 'heading_1':
      $strParts = $notionBlock->heading_1->rich_text;
      if (count($strParts) === 0) break;

      $richText = '';
      foreach ($strParts as $strPart) {
        $string = addHTMLTagsToNotionStrPart($strPart);
        
        $richText = $richText . $string;
      }
      $richText = $richText;

      $preparedBlock = new Kirby\Cms\Block([
        'content' => [
          'level' => 'h1',
          'text' => nl2br($richText),
          'zindex' => '0'
        ],
        'id' => $notionBlock->id,
        'type' => 'heading'
      ]);
      break;
    
    case 'heading_2':
      $strParts = $notionBlock->heading_2->rich_text;
      if (count($strParts) === 0) break;

      $richText = '';
      foreach ($strParts as $strPart) {
        $string = addHTMLTagsToNotionStrPart($strPart);
        
        $richText = $richText . $string;
      }
      $richText = $richText;

      $preparedBlock = new Kirby\Cms\Block([
        'content' => [
          'level' => 'h2',
          'text' => nl2br($richText),
          'zindex' => '0'
        ],
        'id' => $notionBlock->id,
        'type' => 'heading'
      ]);
      break;
    
    case 'heading_3':
      $strParts = $notionBlock->heading_3->rich_text;
      if (count($strParts) === 0) break;

      $richText = '';
      foreach ($strParts as $strPart) {
        $string = addHTMLTagsToNotionStrPart($strPart);
        
        $richText = $richText . $string;
      }
      $richText = $richText;

      $preparedBlock = new Kirby\Cms\Block([
        'content' => [
          'level' => 'h3',
          'text' => nl2br($richText),
          'zindex' => '0'
        ],
        'id' => $notionBlock->id,
        'type' => 'heading'
      ]);
      break;
  case 'heading_4':
      $strParts = $notionBlock->heading_4->rich_text;
      if (count($strParts) === 0) break;
      
      $richText = '';
      foreach ($strParts as $strPart) {
        $string = addHTMLTagsToNotionStrPart($strPart);
        
        $richText = $richText . $string;
      }
      $richText = $richText;

      $preparedBlock = new Kirby\Cms\Block([
        'content' => [
          'level' => 'h4',
          'text' => nl2br($richText),
          'zindex' => '0'
        ],
        'id' => $notionBlock->id,
        'type' => 'heading'
      ]);
      break;
    
    case 'paragraph':
      $strParts = $notionBlock->paragraph->rich_text; 
      if (count($strParts) === 0) break;

      $richText = '';
      foreach ($strParts as $strPart) {
        $string = addHTMLTagsToNotionStrPart($strPart);
        
        $richText = $richText . $string;
      }
      $richText = $richText;

      $preparedBlock = new Kirby\Cms\Block([
        'content' => [
          'text' => nl2br($richText),
          'zindex' => '0'
        ],
        'id' => $notionBlock->id,
        'type' => 'text'
      ]);
      break;
    
    case 'divider':
      $preparedBlock = new Kirby\Cms\Block([
        'id' => $notionBlock->id,
        'type' => 'line'
      ]);
      break;
    
    case 'child_page':
      if ($target === 'subpages') {
        //CREATE CHILD PAGE
        $childPage = $page->createChild([
          'id' => $notionBlock->id,
          'slug' => Str::slug($notionBlock->child_page->title),
          'template' => 'composition',
          'content' => [
            'title' => $notionBlock->child_page->title
          ]
        ]);
        
        // CREATE BLOCK LINK
        $preparedBlock = new Kirby\Cms\Block([
          'id' => $notionBlock->id,
          'type' => 'text',
          'content' => [
            'text' => '<a href="' . $childPage->url() . '">' . $notionBlock->child_page->title . '</a>'
          ]
        ]);

        $url = 'https://api.notion.com/v1/blocks/' . $notionBlock->id . '/children?page_size=100';

        $headers = [
          'Notion-Version: ' . (string)$api->version(),
          'Authorization: Bearer ' . (string)$api->key()
        ];

        //QUERY API
        $curl = curl_init();
        $json = getNotionBlocks($curl, $url, $headers);
        curl_close($curl);

        createJSONFile($childPage, $notionBlock->id, $json);

        $response = json_decode($json);
        $incomingBlocks = $response->results;

        $preparedBlocks = prepareBlocksRecursively($incomingBlocks, $api, $childPage, $target);

        $childPage->update([
          'mode' => 'composition',
          'apiPageId' => $notionBlock->id,
          'composition' => json_encode($preparedBlocks)
        ]);

        $childPage->publish();
      }
      break;
  }
  return $preparedBlock;
}