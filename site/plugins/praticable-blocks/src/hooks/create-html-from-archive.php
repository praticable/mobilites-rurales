<?php

if (!is_dir($kirby->root('assets') . '/static-data')) {
  mkdir($kirby->root('assets') . '/static-data');
  if (!is_dir($kirby->root('assets') . '/static-data/' . $page->slug())) {
    mkdir($kirby->root('assets') . '/static-data/' . $page->slug());
  }
}
$to = $kirby->root('assets') . '/static-data/' . $page->slug() . '/' . $file->name();

F::unzip($file->root(), $to);

// RENAME INDEX.HTML
$files = scandir($to);
$firstFile = $files[2]; // because [0] = "." [1] = ".." 
$originalIndexFile = $to . '/' . $firstFile . '.html';
$newIndexFile = $to . '/' . 'index.html';
rename($originalIndexFile, $newIndexFile);

// CLEAN HTML FILE : REMOVE <HEAD></HEAD> AND <BODY></BODY>
$html = file_get_contents($newIndexFile);
$html = cleanHTML($html);
$staticDataRoot = $site->url() . '/assets/static-data/' . $page->slug() . '/' . $file->name() . '/';
$html = str_replace('src="', 'src="' . $staticDataRoot, $html);
file_put_contents($newIndexFile, $html);