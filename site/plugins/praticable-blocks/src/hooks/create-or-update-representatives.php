<?php

if ($newPage->isPublished() != 'true') return;

$parent = $newPage->parent();

//================================================================ CREATE REPRESENTATIVE IN PARENT
if ($parent !== null) {
  $isRepresentativeEnable = $newPage->disableRepresentative()->exists() != 1 || $newPage->disableRepresentative() != 'true';
  $existingRepresentative = $parent->composition()->toBlocks()->findBy('title', $newPage->title()->value());
  
  if ($isRepresentativeEnable) {
    
    $linkedSpaces = [];
    // Enable to display linkedSpaces
    // foreach ($parent->linkedSpaces()->toPages() as $linkedSpace) {
    //   $linkedSpaces[] = $linkedSpace->uri();
    // }
    foreach ($newPage->children() as $child) {
      $linkedSpaces[] = $child->uri();
    }
    $params = [
      'representedPage' => $newPage,
      'blocks' => $parent->composition()->toBlocks(),
      'existingRepresentative' => $existingRepresentative,
      'linkedSpaces' => $linkedSpaces
    ];
    $parentComposition = addRepresentativeToBlocks($params);
  } else {    
    if ($existingRepresentative) {
      $parentComposition = $parent->composition()->toBlocks()->remove($existingRepresentative->id());    
    } else {
      $parentComposition = $parent->composition()->toBlocks();
    }
  }

  $parent->update([
    'composition' => json_encode($parentComposition->toArray())
  ]);
}

//================================================================ CREATE REPRESENTATIVES FOR LINKED PAGES
if ($newPage->linkedSpaces()->isNotEmpty()) {    
  foreach ($newPage->linkedSpaces()->toPages() as $linkedSpace) {
    $isRepresentativeEnable = $linkedSpace->disableRepresentative()->exists() != 1 || $linkedSpace->disableRepresentative() != 'true';
    $existingRepresentative = $newPage->composition()->toBlocks()->findBy('title', $linkedSpace->title()->value());    
    

    if ($isRepresentativeEnable) {
      $params = [
        'representedPage' => $linkedSpace,
        'blocks' => $newPage->composition()->toBlocks(),
        'existingRepresentative' => $existingRepresentative,
        'linkedSpaces' => $linkedSpace->linkedSpaces()->children()->toPages()
      ];
      $newPageComposition = addRepresentativeToBlocks($params);
    } else {
      if ($existingRepresentative) {
        $newPageComposition = $newPage->composition()->toBlocks()->remove($existingRepresentative->id());
      } else {
        $newPageComposition = $newPage->composition()->toBlocks();
      }
    }

    $newPage = $newPage->update([
      'composition' => json_encode($newPageComposition->toArray())
    ]);
  }
}