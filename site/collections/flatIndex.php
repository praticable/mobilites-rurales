<?php

return function($site) {
  $flatIndex = [];
  foreach ($site->index()->published() as $space) {
    $itemIndex = [
      'title' => $space->title()->value(),
      'uri' => $space->uri(),
      'url' => $space->url(),
      'isDesktopOnly' => $space->isDesktopOnly()->isEmpty() ? false : $space->isDesktopOnly()->toBool(),
      'isExternal' => $space->isExternal()->isEmpty() ? false : $space->isExternal()->toBool(),
      'parentUri' => false,
      'children' => null
    ];

    if ($space->isHomePage() == false) {
      $itemIndex['parentUri'] = $space->parent() ? $space->parent()->uri() : $site->homePage()->uri();
    }

    foreach ($space->children() as $child) {
      $itemIndex['children'][] = [
        'title' => $child->title()->value(),
        'uri' => $child->uri(),
        'url' => $child->url(),
        'isDesktopOnly' => $child->isDesktopOnly()->isEmpty() ? false : $child->isDesktopOnly()->toBool(),
        'isExternal' => $child->isExternal()->isEmpty() ? false : $child->isExternal()->toBool(),
        'hasChildren' => $child->hasChildren()
      ];
    }

    if ($space->isHomePage()) {      
      foreach ($site->children()->listed()->without('home') as $child) {
        $itemIndex['children'][] = [
          'title' => $child->title()->value(),
          'uri' => $child->uri(),
          'url' => $child->url(),
          'isDesktopOnly' => $child->isDesktopOnly()->isEmpty() ? false : $child->isDesktopOnly()->toBool(),
          'isExternal' => $child->isExternal()->isEmpty() ? false : $child->isExternal()->toBool(),
          'hasChildren' => $child->hasChildren()
        ];
      }
    }

    $flatIndex[] = $itemIndex;
    
  };
  return json_encode($flatIndex);
};