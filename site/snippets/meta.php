<?php 
$pageTitle = $page->seoTitle()->isNotEmpty() ? $page->seoTitle() : $page->title();
$title = $page->isHomePage() ? $site->title() : $pageTitle . " | " . $site->title();
$description = $page->isHomePage() ? $site->description() : $page->description();
$siteCover = $site->cover()->isNotEmpty() ? $site->cover()->toFile()->url() : false;
$pageCover = $page->coverThumb()->isNotEmpty() ? $page->coverThumb() : false;
$image = $page->isHomePage() ? $siteCover : $pageCover;
?>

<title>DEV - <?= $title ?></title>
<?php if ($description->isNotEmpty()): ?>
<meta name="description" content="<?= $description ?>">
<?php endif ?>
<meta name="generator" content="Kirby (<?= Kirby::version() ?>)">

<!--========== TWITTER ==========-->
<meta name="twitter:card" content="<?= $image ? "summary_large_image" : "summary" ?>">
<meta name="twitter:title" content="<?= $title ?>">
<?php if ($description->isNotEmpty()): ?>
<meta name="twitter:description" content="<?= $description ?>">
<?php endif ?>
<?php if ($image): ?>
<meta property="og:image" content="<?= $image ?>">
<?php endif ?>
<?php if ($site->twitter()->isNotEmpty()): ?>
<meta name="twitter:site" content="@<?= $site->twitter() ?>">
<?php endif ?>

<!--========== OG GRAPH ==========-->
<meta property="og:title" content="<?= $title ?>">
<meta property="og:site_name" content="<?= $site->title() ?>">
<?php if ($description->isNotEmpty()): ?>
<meta property="og:description" content="<?= $description ?>">
<?php endif ?>
<meta property="og:url" content="<?= $page->url() ?>">
<meta property="og:type" content="<?= $page->isHomePage() ? "website" :  "article"  ?>">
<?php if ($page->author()->isNotEmpty()): ?>
<meta name="author" content="<?= $page->author() ?>">
<meta property="article:author" content="<?= $page->author() ?>">
<?php endif ?>
<?php if ($page->published()->isNotEmpty()): ?>
<meta name="date" content="<?= $page->published() ?>" scheme="YYYY-MM-DD">
<meta property="article:published_time" content="<?= $page->published() ?>">
<?php endif ?>
<?php if ($page->tag()->isNotEmpty()): ?>
<meta property="article:tag" content="<?= $page->tag() ?>">
<?php endif ?>
<?php if ($image): ?>
<meta property="twitter:image" content="<?= $image ?>">
<?php endif ?>

<!--========== SCHEMA ==========-->
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "BreadcrumbList",
    "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
        "@id": "<?= $site->homePage()->url() ?>",
        "name": "<?= $site->title() ?>"
    }
    }<?php if (!$page->isHomePage() && $page->depth() === 1): ?>,
    {
    "@type": "ListItem",
    "position": 2,
    "item": 
    {
        "@id": "<?= $page->url() ?>",
        "name": "<?= $page->title() ?>"
    }
    }<?php endif ?><?php if ($page->depth() === 2): ?>,
    {
    "@type": "ListItem",
    "position": 2,
    "item": 
    {
        "@id": "<?= $page->parent()->url() ?>",
        "name": "<?= $page->parent()->title() ?>"
    }
    },
    {
    "@type": "ListItem",
    "position": 3,
    "item": 
    {
        "@id": "<?= $page->url() ?>",
        "name": "<?= $page->title() ?>"
    }
    }<?php endif ?>
    ]
}
</script>
<?php if (!$page->isHomePage()): ?>
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "Article",
    "name": "<?= $page->title() ?>"<?php if ($page->author()->isNotEmpty()): ?>,
    "author": "John Doe"<?php endif ?><?php if ($page->published()->isNotEmpty()): ?>,
    "datePublished": "<?= $page->published() ?>"<?php endif ?><?php if ($page->description()->isNotEmpty()): ?>,
    "headline": "<?= $page->description() ?>"<?php endif ?><?php if ($image): ?>,
    "image": "<?= $image ?>"<?php endif ?>  
}
</script>
<?php endif ?>