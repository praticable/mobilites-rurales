<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
    <?php snippet('meta') ?>
    
    <link rel="icon" type="image/x-icon" href="<?= url('assets/images/icons/favicon.svg') ?>">
    
    <!-- à remplacer quand le site sera en prod par "index,follow" -->
    <meta name="robots" content="noindex, nofollow, noarchive"> 
    <link rel="canonical" href="<?= $page->url() ?>">



    <link rel="apple-touch-icon" sizes="180x180" href="<?= url('assets/favicon') ?>/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= url('assets/favicon') ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= url('assets/favicon') ?>/favicon-16x16.png">
    <link rel="manifest" href="<?= url('assets/favicon') ?>/site.webmanifest">
    <link rel="mask-icon" href="<?= url('assets/favicon') ?>/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <!--========== INTERACT ==========-->
    <script src="<?= url('assets/js/libs/interact.min.js') ?>"></script>
    <script src="<?= url('assets/js/libs/vue-interactjs.umd.js') ?>"></script>
    
    <!--========== VUE ==========-->
    <script src="<?= url('assets/js/libs/vue.js') ?>"></script>

    <!--========== DEVELOPMENT : RAW ==========-->
    <script src="<?= url('assets/js/app.js') ?>" type="module" defer></script>
    <link rel="stylesheet" href="<?= url('assets/css/praticable/style.css?version-cache-prevent') . rand(0, 1000) ?>">
    <?php if (file_exists(__DIR__ . '/../../assets/css/theme.css')): ?>
      <link rel="stylesheet" href="<?= url('assets/css/theme.css?version-cache-prevent') . rand(0, 1000) ?>">
    <?php endif ?>

    <!--========== PRODUCTION : BUNDLES ==========-->
    <!-- <script src="<?= url('assets/dist/app.bundle.js') ?>" defer></script> -->
    <!-- <link rel="stylesheet" href="<?= url('assets/dist/style.css') ?>" -->
    <!--
    <?php if (file_exists(__DIR__ . '/../../assets/css/theme.css')): ?>
      <link rel="stylesheet" href="<?= url('assets/css/theme.css') ?>">
    <?php endif ?>
    -->
    
    <!--========== MARKEDJS (MARKDOWN PARSER) ==========-->
    <script src="<?= url('assets/js/libs/marked.min.js') ?>"></script>

    <!--========== FONTS ==========-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300..700&display=swap" rel="stylesheet">
</head>
<body>
    <?php snippet('svg') ?>